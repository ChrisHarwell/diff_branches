
extern crate git2;

use git2::{Repository, DiffOptions, DiffFormat, Delta};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Open the current repository
    let repo = Repository::open(".")?;

    // Get the name of the current branch
    let head = repo.head()?;
    let current_branch = head.name().unwrap_or("HEAD");

    // Get the main branch
    let main_branch = "master"; // or "master" if using the master branch

    // Get the diff between the current branch and main branch
    let diff = {
        let tree1 = repo.revparse_single(&format!("{}", main_branch))?.peel_to_tree()?;
        let tree2 = repo.revparse_single(&format!("{}", current_branch))?.peel_to_tree()?;
        repo.diff_tree_to_tree(Some(&tree1), Some(&tree2), None)?
    };

    // Create a diff options structure
    let mut diff_opts = DiffOptions::new();
    diff_opts.include_untracked(true);

    // Generate the diff and save it to a file
    let mut diff_text = String::new();
    diff.print(DiffFormat::Patch, |delta, hunk, _| {
        let delta_str = match delta.status() {
            Delta::Added => "Added",
            Delta::Deleted => "Deleted",
            Delta::Modified => "Modified",
            _ => "Unchanged",
        };

        let hunk_str = hunk.map(|h| format!(" {:?}", h)).unwrap_or_default();

        diff_text.push_str(&format!("Delta: {}\nHunk:{}\n", delta_str, hunk_str));
        true
    })?;

    std::fs::write("git_diff.txt", diff_text)?;

    println!("Git diff saved to git_diff.txt");

    Ok(())
}

